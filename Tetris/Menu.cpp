#include <iostream>
#include <ctime>
#include <conio.h>

#include "windows.h"

#include "Rating.h"
#include "Game.h"

using namespace std;

const int sizeMenuY = 3;
const int sizeMenuX = 11;
char menuUser[sizeMenuY][sizeMenuX];

int cursorPosition = 0;
int indexMenu = 0;
int index = 0;

//indexMenu = 1
char menuMain[sizeMenuY][sizeMenuX] = {
	{'>', ' ', 'G', 'a', 'm', 'e',},
	{' ', ' ', 'R', 'a', 't', 'i', 'n', 'g', },
	{' ', ' ', 'E', 'x', 'i', 't',},
};

//indexMenu = 2
char menuSubmenu[sizeMenuY][sizeMenuX] = {
	{'>', ' ', 'N', 'a', 'w', ' ', 'g', 'a', 'm', 'e',},
	{' ', ' ', 'S', 'a', 'v', 'e', ' ', 'g', 'a', 'm', 'e',},
	{' ', ' ', 'E', 'x', 'i', 't',},
};

void logo()
{
	srand(time(0));
	const string fileName = "logo.txt";

	ifstream fin;
	fin.open(fileName);
	bool isOpen = fin.is_open();

	if (isOpen == true)
	{
		string str;
		while (!fin.eof())
		{
			str = "";
			getline(fin, str);
			Sleep(rand() % 1000 + 10);
			cout << "    " << str << endl;
		}
	}
	else
	{
		cout << "Error: could not open the file" << endl;;
	}
	fin.close();

	system("pause");
	system("cls");
}
void buildingMenu() {
	for (int i = 0; i < sizeMenuY; i++)
	{
		for (int j = 0; j < sizeMenuX; j++)
		{
			if (indexMenu == 0)
			{
				menuUser[i][j] = menuMain[i][j];
			}
			else if (indexMenu == 1)
			{
				menuUser[i][j] = menuSubmenu[i][j];
			}
		}
	}
}
void showMenu() {
	cout << endl << endl;
	for (int i = 0; i < sizeMenuY; i++)
	{
		cout << "\t\t";
		for (int j = 0; j < sizeMenuX; j++)
		{
			cout << menuUser[i][j];
			if (index == 0)
			{
				Sleep(50);
			}
		}
		cout << endl;
	}
}
void keystrokesMenu()
{
	switch (char click = _getch())
	{
	case 'w':
		if (cursorPosition != 0)
		{
			menuUser[cursorPosition][0] = ' ';
			menuUser[cursorPosition - 1][0] = '>';
			cursorPosition--;
		}
		break;
	case 's':
		if (cursorPosition != 2)
		{
			menuUser[cursorPosition][0] = ' ';
			menuUser[cursorPosition + 1][0] = '>';
			cursorPosition++;
		}
		break;
	case 'f':
		if (indexMenu == 0) {
			if (cursorPosition == 0) {
				index = 0;
				indexMenu = 1;
				cursorPosition = 0;
				buildingMenu();
				break;
			}
			else if (cursorPosition == 1) {
				system("cls");
				showRating();
				break;
			}
			else if (cursorPosition == 2)
			{
				exit(0);
				break;
			}
		}
		if (indexMenu == 1)
		{
			if (cursorPosition == 0)
			{
				loading(cursorPosition);
				figureRand();
				game();
				break;
			}
			if (cursorPosition == 1)
			{
				loading(cursorPosition);
				game();
				break;
			}
			else if (cursorPosition == 2) {
				index = 0;
				indexMenu = 0;
				cursorPosition = 0;
				buildingMenu();
				break;
			}
		}
		break;
	default:
		break;
	}
}
int main()
{
	logo();
	initRating();
	buildingMenu();
	do
	{
		showMenu();
		index++;
		keystrokesMenu();
		system("cls");
	} while (true);
	return 0;
}