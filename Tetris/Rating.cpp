#include "Rating.h"

Rating* Ratings;
int countRating = 0;

void initRating()
{
	string fileName = "rating.txt";
	ifstream fin;
	fin.open(fileName);
	bool isOpen = fin.is_open();
	if (isOpen == true) {
		while (!fin.eof()) {
			fin >> countRating;
			Ratings = new Rating[countRating];
			for (int i = 0; i < countRating; i++) {
				fin >> Ratings[i].name;
				fin >> Ratings[i].score;
			}
			break;
		}
	}
	else {
		cout << "Error: could not open the file";
	}
}

void addRating(string name, int score)
{
	const string fileName = "rating.txt";
	ofstream fout;
	fout.open(fileName);

	Rating* temp = new Rating[countRating + 1];
	for (int i = 0; i < countRating; i++)
	{
		temp[i] = Ratings[i];
	}

	temp[countRating].name = name;
	temp[countRating].score = score;
	countRating++;

	Ratings = new Rating[countRating];
	for (int i = 0; i < countRating; i++)
	{
		Ratings[i] = temp[i];
	}
	delete[]temp;

	if (fout.is_open() == true) {
		fout << countRating << endl;
		for (int i = 0; i < countRating; i++)
		{
			fout << Ratings[i].name << endl;
			fout << Ratings[i].score << endl;
		}
	}
	else
	{
		cout << "Error: could not open the file";
	}
	fout.close();
}

void sortingRating() {
	for (int i = 0; i < countRating - 1; i++) {
		for (int j = 0; j < countRating - 1; j++) {
			if (Ratings[j].score < Ratings[j + 1].score) {
				Rating temp = Ratings[j];
				Ratings[j] = Ratings[j + 1];
				Ratings[j + 1] = temp;
			}
		}
	}
}

void showRating()
{
	sortingRating();
	for (int i = 0; i < 10; i++)
	{
		cout << i + 1 << ". " << Ratings[i].name << " - " << Ratings[i].score << endl;
	}
	system("pause");
}