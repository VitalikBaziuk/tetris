#pragma once
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

struct Rating
{
	string name;
	int score;
};

void initRating();
void addRating(string name, int score);
void sortingRating();
void showRating();