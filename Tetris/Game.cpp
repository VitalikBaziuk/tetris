﻿#include <iostream>
#include <ctime>
#include <fstream>
#include <string>

#include <conio.h>
#include <stdio.h>
#include <stdlib.h>

#include "Color.h"
#include "Game.h"
#include "Rating.h"

using namespace std;

const int sizeMapY = 25;
const int sizeMapX = 30;

int map[sizeMapY][sizeMapX];

const int sizeFigure = 8; //Розмір фігури (кіль-ть блоків)
string name = "";
int score = 0;
int line = 0;

struct figure
{
	int Y[sizeFigure] = { 0, 0, 0, 0, 0, 0, 0, 0, };
	int X[sizeFigure] = { 13, 13, 13, 13, 13, 13, 13, 13, };
	int tetramino;
	int orientation;
	int color;
	int number;
};
figure* figures;
int countFigure = 0;

/*Вивод масива*/
void showGame()
{
	cout << endl << endl;
	for (int i = 0; i < sizeMapY; i++)
	{
		cout << "\t\t\t\t\t";
		for (int j = 0; j < sizeMapX; j++)
		{
			if (map[i][j] == 0)
			{
				cout << " ";
			}
			else if (map[i][j] < 0) {
				if (map[i][j] == -1)
				{
					cout << (char)186;
				}
				else if (map[i][j] == -2)
				{
					cout << (char)205;
				}
				else if (map[i][j] == -3)
				{
					cout << (char)201;
				}
				else if (map[i][j] == -4)
				{
					cout << (char)187;
				}
				else if (map[i][j] == -5)
				{
					cout << (char)200;
				}
				else if (map[i][j] == -6)
				{
					cout << (char)188;
				}
				else if (map[i][j] == -10)
				{
					cout << (char)179;
				}
				else if (map[i][j] == -11)
				{
					cout << (char)218;
				}
				else if (map[i][j] == -12)
				{
					cout << (char)196;
				}
				else if (map[i][j] == -13)
				{
					cout << (char)191;
				}
				else if (map[i][j] == -14)
				{
					cout << (char)194;
				}
				else if (map[i][j] == -15)
				{
					cout << (char)195;
				}
				else if (map[i][j] == -16)
				{
					cout << (char)180;
				}
				else if (map[i][j] == -17)
				{
					cout << (char)192;
				}
				else if (map[i][j] == -18)
				{
					cout << (char)217;
				}
				else if (map[i][j] == -19)
				{
					cout << (char)193;
				}
			}
			else if (map[i][j] == figures[countFigure].number)
			{
				if (figures[countFigure].color == 1)
				{
					colorGreen();
				}
				else if (figures[countFigure].color == 2)
				{
					colorRed();
				}
				else if (figures[countFigure].color == 3)
				{
					colorBlue();
				}
				else if (figures[countFigure].color == 4)
				{
					colorPurple();
				}
				else if (figures[countFigure].color == 5)
				{
					colorYellow();
				}
				cout << (char)219;
			}
			else
			{
				for (int c = 0; c < countFigure; c++)
				{
					for (int s = 0; s < sizeFigure; s++)
					{
						if (map[i][j] == map[figures[c].Y[s]][figures[c].X[s]] && figures[c].color == 1)
						{
							colorGreen();
						}
						else if (map[i][j] == map[figures[c].Y[s]][figures[c].X[s]] && figures[c].color == 2)
						{
							colorRed();
						}
						else if (map[i][j] == map[figures[c].Y[s]][figures[c].X[s]] && figures[c].color == 3)
						{
							colorBlue();
						}
						else if (map[i][j] == map[figures[c].Y[s]][figures[c].X[s]] && figures[c].color == 4)
						{
							colorPurple();
						}
						else if (map[i][j] == map[figures[c].Y[s]][figures[c].X[s]] && figures[c].color == 5)
						{
							colorYellow();
						}
					}
				}
				cout << (char)219;
			}
			colorLightGrey();
		}
		cout << endl;
	}
	cout << "\t\t\t\t\t    " << "score: " << score << "\t" << "line: " << line << endl;
}

/**/
void gameOver() {
	system("cls");
	const int sizeY = 3;
	const int sizeX = 28;
	int gameover[sizeY][sizeX] = {
		{-11,-12,-13,-11,-12,-13,-11,-14,-13,-11,-12,-13,0,0,-11,-12,-13,0,-14,  0,  0,-14,-11,-12,-13,-14,-12,-13,},
		{-10,  0,-14,-15,-12,-16,-10,-10,-10,-15,-16,  0,0,0,-10,  0,-10,0,-17,-13,-11,-18,-15,-16,  0,-15,-14,-18,},
		{-17,-12,-18,-19,  0,-19,-19,  0,-19,-17,-12,-18,0,0,-17,-12,-18,0,  0,-17,-18,  0,-17,-12,-18,-19,-17,-12,},
	};
	for (int i = 0; i < sizeY; i++)
	{
		for (int j = 0; j < sizeX; j++)
		{
			map[i + (sizeMapY - sizeY) / 2][j + 1] = gameover[i][j];
		}
	}
	showGame();
	cout << "\t\t\t\t\t\t" << "Enter your name: ";
	cin >> name;
	addRating(name, score);
}

/*Вивод фігур*/
void construction(int Y[], int X[])
{
	for (int i = 0; i < sizeFigure; i++)
	{
		figures[countFigure].Y[i] += Y[i];
		figures[countFigure].X[i] += X[i];
	}
}

/*	figure1 = O figure2 = I figure3 = Z figure4 = S figure5 = J figure6 = L igure7 = T*/
void constructionFigure()
{
	if (figures[countFigure].tetramino == 1)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 1, 1, 1, 1, 2, 2, 2, 2, };
			int X[sizeFigure] = { 0, 1, 2, 3, 0, 1, 2, 3, };
			construction(Y, X);
		}
	}
	else if (figures[countFigure].tetramino == 2)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 1, 1, 1, 1, 1, 1, 1, 1, };
			int X[sizeFigure] = { -4, -3, -2, -1, 0, 1, 2, 3, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 1, 1, 2, 2, 3, 3, 4, 4, };
			int X[sizeFigure] = { 0, 1, 0, 1, 0, 1, 0, 1, };
			construction(Y, X);
		}
	}
	else if (figures[countFigure].tetramino == 3)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 1, 1, 1, 1, 2, 2, 2, 2, };
			int X[sizeFigure] = { -2, -1, 0, 1, 0, 1, 2, 3, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 1, 1, 2, 2, 2, 2, 3, 3, };
			int X[sizeFigure] = { 0, 1, 0, 1, -1, -2, -1, -2, };
			construction(Y, X);
		}
	}
	else if (figures[countFigure].tetramino == 4)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 2, 2, 2, 2, 1, 1, 1, 1, };
			int X[sizeFigure] = { -2, -1, 0, 1, 0, 1, 2, 3, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 1, 1, 2, 2, 2, 2, 3, 3, };
			int X[sizeFigure] = { 0, 1, 0, 1, 2, 3, 2, 3, };
			construction(Y, X);
		}
	}
	else if (figures[countFigure].tetramino == 5)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 1, 1, 2, 2, 3, 3, 3, 3, };
			int X[sizeFigure] = { 0, 1, 0, 1, 0, 1, -2, -1, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 2, 2, 2, 2, 2, 2, 1, 1, };
			int X[sizeFigure] = { 2, 3, 0, 1, -2, -1, -2, -1, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 3)
		{
			int Y[sizeFigure] = { 3, 3, 2, 2, 1, 1, 1, 1, };
			int X[sizeFigure] = { 0, 1, 0, 1, 0, 1, 2, 3, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 4)
		{
			int Y[sizeFigure] = { 1, 1, 1, 1, 1, 1, 2, 2, };
			int X[sizeFigure] = { -2, -1, 0, 1, 2, 3, 2, 3, };
			construction(Y, X);
		}
	}
	else if (figures[countFigure].tetramino == 6)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 1, 1, 2, 2, 3, 3, 3, 3, };
			int X[sizeFigure] = { 0, 1, 0, 1, 0, 1, 2, 3, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 2, 2, 2, 2, 2, 2, 1, 1, };
			int X[sizeFigure] = { -2, -1, 0, 1, 2, 3, 2, 3, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 3)
		{
			int Y[sizeFigure] = { 3, 3, 2, 2, 1, 1, 1, 1, };
			int X[sizeFigure] = { 0, 1, 0, 1, 0, 1, -2, -1, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 4)
		{
			int Y[sizeFigure] = { 1, 1, 1, 1, 1, 1, 2, 2, };
			int X[sizeFigure] = { 2, 3, 0, 1, -2, -1, -2, -1, };
			construction(Y, X);
		}
	}
	else if (figures[countFigure].tetramino == 7)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 1, 1, 2, 2, 2, 2, 2, 2, };
			int X[sizeFigure] = { 0, 1, -2, -1, 0, 1, 2, 3, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 2, 2, 1, 1, 2, 2, 3, 3, };
			int X[sizeFigure] = { 2, 3, 0, 1, 0, 1, 0, 1, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 3)
		{
			int Y[sizeFigure] = { 2, 2, 1, 1, 1, 1, 1, 1, };
			int X[sizeFigure] = { 0, 1, 2, 3, 0, 1, -2, -1, };
			construction(Y, X);
		}
		else if (figures[countFigure].orientation == 4)
		{
			int Y[sizeFigure] = { 2, 2, 3, 3, 2, 2, 1, 1, };
			int X[sizeFigure] = { -2, -1, 0, 1, 0, 1, 0, 1, };
			construction(Y, X);
		}
	}
}

/*Зміна положження фігури*/
void orientationFigure()
{
	if (figures[countFigure].tetramino == 1)
	{
		if (figures[countFigure].orientation == 1) {

		}
	}
	else if (figures[countFigure].tetramino == 2)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 0, 0, 1, 1, 2, 2, 3, 3, };
			int X[sizeFigure] = { 0, 0, -2, -2, -4, -4, -6, -6, };
			construction(Y, X);
			figures[countFigure].orientation = 2;
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 0, 0, -1, -1, -2, -2, -3, -3, };
			int X[sizeFigure] = { 0, 0, 2, 2, 4, 4, 6, 6, };
			construction(Y, X);
			figures[countFigure].orientation = 1;
		}
	}
	else if (figures[countFigure].tetramino == 3)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 0, 0, 1, 1, 0, 0, 1, 1, };
			int X[sizeFigure] = { 4, 4, 2, 2, 0, 0, -2, -2, };
			construction(Y, X);
			figures[countFigure].orientation = 2;
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 0, 0, -1, -1, 0, 0, -1, -1, };
			int X[sizeFigure] = { -4, -4, -2, -2, 0, 0, 2, 2, };
			construction(Y, X);
			figures[countFigure].orientation = 1;
		}
	}
	else if (figures[countFigure].tetramino == 4)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { -1, -1, 0, 0, 1, 1, 2, 2, };
			int X[sizeFigure] = { 0, 0, -2, -2, 0, 0, -2, -2, };
			construction(Y, X);
			figures[countFigure].orientation = 2;
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 1, 1, 0, 0, -1, -1, -2, -2, };
			int X[sizeFigure] = { 0, 0, 2, 2, 0, 0, 2, 2, };
			construction(Y, X);
			figures[countFigure].orientation = 1;
		}
	}
	else if (figures[countFigure].tetramino == 5)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 2, 2, 1, 1, 0, 0, -1, -1, };
			int X[sizeFigure] = { 2, 2, 0, 0, -2, -2, 0, 0, };
			construction(Y, X);
			figures[countFigure].orientation = 2;
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 0, 0, -1, -1, -2, -2, -1, -1, };
			int X[sizeFigure] = { -2, -2, 0, 0, 2, 2, 4, 4, };
			construction(Y, X);
			figures[countFigure].orientation = 3;
		}
		else if (figures[countFigure].orientation == 3)
		{
			int Y[sizeFigure] = { -1, -1, 0, 0, 1, 1, 2, 2, };
			int X[sizeFigure] = { -2, -2, 0, 0, +2, +2, 0, 0, };
			construction(Y, X);
			figures[countFigure].orientation = 4;
		}
		else if (figures[countFigure].orientation == 4)
		{
			int Y[sizeFigure] = { -1, -1, 0, 0, 1, 1, 0, 0, };
			int X[sizeFigure] = { 2, 2, 0, 0, -2, -2, -4, -4, };
			construction(Y, X);
			figures[countFigure].orientation = 1;
		}
	}
	else if (figures[countFigure].tetramino == 6)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 2, 2, 1, 1, 0, 0, -1, -1, };
			int X[sizeFigure] = { 0, 0, 2, 2, 4, 4, 2, 2, };
			construction(Y, X);
			figures[countFigure].orientation = 2;
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 0, 0, -1, -1, -2, -2, -1, -1, };
			int X[sizeFigure] = { 4, 4, 2, 2, 0, 0, -2, -2, };
			construction(Y, X);
			figures[countFigure].orientation = 3;
		}
		else if (figures[countFigure].orientation == 3)
		{
			int Y[sizeFigure] = { -1, -1, 0, 0, 1, 1, 2, 2, };
			int X[sizeFigure] = { 0, 0, -2, -2, -4, -4, -2, -2, };
			construction(Y, X);
			figures[countFigure].orientation = 4;
		}
		else if (figures[countFigure].orientation == 4)
		{
			int Y[sizeFigure] = { -1, -1, 0, 0, 1, 1, 0, 0, };
			int X[sizeFigure] = { -4, -4, -2, -2, 0, 0, 2, 2, };
			construction(Y, X);
			figures[countFigure].orientation = 1;
		}
	}
	else if (figures[countFigure].tetramino == 7)
	{
		if (figures[countFigure].orientation == 1)
		{
			int Y[sizeFigure] = { 1, 1, -1, -1, 0, 0, 1, 1, };
			int X[sizeFigure] = { 2, 2, 2, 2, 0, 0, -2, -2, };
			construction(Y, X);
			figures[countFigure].orientation = 2;
		}
		else if (figures[countFigure].orientation == 2)
		{
			int Y[sizeFigure] = { 0, 0, 0, 0, -1, -1, -2, -2, };
			int X[sizeFigure] = { -2, -2, -2, -2, 0, 0, 2, 2, };
			construction(Y, X);
			figures[countFigure].orientation = 3;
		}
		else if (figures[countFigure].orientation == 3)
		{
			int Y[sizeFigure] = { 0, 0, 2, 2, 1, 1, 0, 0, };
			int X[sizeFigure] = { -2, -2, 2, 2, 0, 0, -2, -2, };
			construction(Y, X);
			figures[countFigure].orientation = 4;
		}
		else if (figures[countFigure].orientation == 4)
		{
			int Y[sizeFigure] = { -1, -1, -1, -1, 0, 0, 1, 1, };
			int X[sizeFigure] = { 2, 2, -2, -2, 0, 0, 2, 2, };
			construction(Y, X);
			figures[countFigure].orientation = 1;
		}
	}
}

/*створення фігури*/
void figureRand()
{
	figure* temp = new figure[countFigure + 1];
	for (int i = 0; i < countFigure; i++)
	{
		temp[i] = figures[i];
	}

	srand(time(0));
	temp[countFigure].tetramino = rand() % 7 + 1;
	if (temp[countFigure].tetramino == 1)
	{
		temp[countFigure].orientation = 1;
	}
	else if (temp[countFigure].tetramino >= 2 || temp[countFigure].tetramino <= 4)
	{
		temp[countFigure].orientation = rand() % 2 + 1;
	}
	else if (temp[countFigure].tetramino >= 5 || temp[countFigure].tetramino <= 7)
	{
		temp[countFigure].orientation = rand() % 4 + 1;
	}

	temp[countFigure].color = rand() % 5 + 1;
	temp[countFigure].number = countFigure + 1;

	figures = new figure[countFigure + 1];
	for (int i = 0; i < countFigure + 1; i++)
	{
		figures[i] = temp[i];
	}
	delete[]temp;

	constructionFigure();
}

/*Перевірка зіткнення*/
int сollisionRight(int countCollision)
{
	for (int i = 0; i < sizeFigure; i++)
	{
		if (map[figures[countFigure].Y[i]][figures[countFigure].X[i] + 2] != 0 && map[figures[countFigure].Y[i]][figures[countFigure].X[i] + 2] != figures[countFigure].number)
		{
			countCollision++;
		}
	}
	return countCollision;
}
int сollisionLeft(int countCollision)
{
	for (int i = 0; i < sizeFigure; i++)
	{
		if (map[figures[countFigure].Y[i]][figures[countFigure].X[i] - 2] != 0 && map[figures[countFigure].Y[i]][figures[countFigure].X[i] - 2] != figures[countFigure].number)
		{
			countCollision++;
		}
	}
	return countCollision;
}
int сollisionDown(int countCollision)
{
	for (int i = 0; i < sizeFigure; i++)
	{
		if (map[figures[countFigure].Y[i] + 1][figures[countFigure].X[i]] != 0 && map[figures[countFigure].Y[i] + 1][figures[countFigure].X[i]] != figures[countFigure].number)
		{
			countCollision++;
		}
	}
	return countCollision;
}

int сollisionText(int countCollision)
{
	figure temp;

	for (int i = 0; i < sizeFigure; i++)
	{
		temp.Y[i] = figures[countFigure].Y[i];
		temp.X[i] = figures[countFigure].X[i];
	}
	for (int i = 0; i < sizeFigure; i++)
	{
		map[figures[countFigure].Y[i]][figures[countFigure].X[i]] = 0;
	}
	orientationFigure();
	for (int i = 0; i < sizeFigure; i++)
	{
		if (map[figures[countFigure].Y[i]][figures[countFigure].X[i]] != 0 && map[figures[countFigure].Y[i]][figures[countFigure].X[i]] != figures[countFigure].number)
		{
			countCollision++;
		}
	}
	for (int i = 0; i < sizeFigure; i++)
	{
		figures[countFigure].Y[i] = temp.Y[i];
		figures[countFigure].X[i] = temp.X[i];
	}
	if (figures[countFigure].tetramino == 1)
	{
		figures[countFigure].orientation = 1;
	}
	else if (figures[countFigure].tetramino >= 2 || figures[countFigure].tetramino <= 4)
	{
		if (figures[countFigure].orientation == 1)
		{
			figures[countFigure].orientation = 2;
		}
		else
		{
			figures[countFigure].orientation--;
		}
	}
	else if (figures[countFigure].tetramino >= 5 || figures[countFigure].tetramino <= 7)
	{
		if (figures[countFigure].orientation == 1)
		{
			figures[countFigure].orientation = 4;
		}
		else
		{
			figures[countFigure].orientation--;
		}
	}
	for (int i = 0; i < sizeFigure; i++)
	{
		map[figures[countFigure].Y[i]][figures[countFigure].X[i]] = figures[countFigure].number;
	}
	return countCollision;
}

/*Очистка лінії*/
void clearLine(int clearX)
{
	score += 280;
	line++;
	for (int i = clearX - 1; i > 1; i--)
	{
		for (int j = 1; j < sizeMapX - 1; j++)
		{
			int temp = map[i][j];
			map[i + 1][j] = temp;
			map[i][j] = 0;
		}
	}
}

/*перевірка ліній*/
void checkLine()
{
	for (int i = 1; i < sizeMapY - 1; i++)
	{
		int countLine = 0;
		for (int j = 1; j < sizeMapX - 1; j++)
		{
			if (map[i][j] != 0 && i == 1)
			{
				gameOver();
			}
			if (map[i][j] != 0)
			{
				countLine++;
			}
		}
		if (countLine == sizeMapX - 2)
		{
			for (int j = 1; j < sizeMapX - 1; j++)
			{
				map[i][j] = 0;
			}
			clearLine(i);
		}
	}
}

/*Збереження гри*/
void saveGame()
{
	const string fileName = "saveGame.txt";
	ofstream fout;
	fout.open(fileName);

	if (fout.is_open() == true) {
		for (int i = 0; i < sizeMapY; i++)
		{
			for (int j = 0; j < sizeMapX; j++)
			{
				fout << map[i][j] << endl;
			}
		}
		fout << score << endl;
		fout << line << endl;
		fout << countFigure << endl;
		for (int i = 0; i < countFigure + 1; i++)
		{
			for (int j = 0; j < sizeFigure; j++)
			{
				fout << figures[i].Y[j] << endl;
			}
			for (int j = 0; j < sizeFigure; j++)
			{
				fout << figures[i].X[j] << endl;
			}
			fout << figures[i].tetramino << endl;
			fout << figures[i].orientation << endl;
			fout << figures[i].color << endl;
			fout << figures[i].number << endl;
		}
	}
	else
	{
		cout << "Error: could not open the file";
	}
	fout.close();
}

/*Загрузка гри*/
void loading(int cursorPosition) {
	if (cursorPosition == 0) {
		string fileName = "newGame.txt";
		ifstream fin;
		fin.open(fileName);
		bool isOpen = fin.is_open();

		if (isOpen == true)
		{
			while (!fin.eof())
			{
				for (int i = 0; i < sizeMapY; i++)
				{
					for (int j = 0; j < sizeMapX; j++)
					{
						fin >> map[i][j];
					}
				}
			}
		}
		else
		{
			cout << "Error: could not open the file" << endl;
		}
		fin.close();
	}
	else if (cursorPosition == 1) {

		string fileName = "saveGame.txt";
		ifstream fin;
		fin.open(fileName);
		bool isOpen = fin.is_open();

		if (isOpen == true)
		{
			while (!fin.eof())
			{
				for (int i = 0; i < sizeMapY; i++)
				{
					for (int j = 0; j < sizeMapX; j++)
					{
						fin >> map[i][j];
					}
				}
				fin >> score;
				fin >> line;
				fin >> countFigure;
				figures = new figure[countFigure + 1];
				for (int i = 0; i < countFigure + 1; i++)
				{
					for (int j = 0; j < sizeFigure; j++)
					{
						fin >> figures[i].Y[j];
					}
					for (int j = 0; j < sizeFigure; j++)
					{
						fin >> figures[i].X[j];
					}
					fin >> figures[i].tetramino;
					fin >> figures[i].orientation;
					fin >> figures[i].color;
					fin >> figures[i].number;
				}
				break;
			}
		}
		else
		{
			cout << "Error: could not open the file" << endl;;
		}
		fin.close();

		for (int i = 0; i < countFigure + 1; i++)
		{
			for (int j = 0; j < sizeFigure; j++) {
				map[figures[i].Y[j]][figures[i].X[j]] = figures[i].number;
			}
		}
	}
}

/*Натискання клавіш / обертання фігури / Рух фігури*/
void keystrokesGame() {
	int countCollision = 0;
	if (_kbhit())
	{
		char click = _getch();
		switch (click)
		{
		case 'd':
			countCollision = сollisionRight(countCollision);
			if (countCollision == 0)
			{
				for (int i = 0; i < sizeFigure; i++)
				{
					map[figures[countFigure].Y[i]][figures[countFigure].X[i]] = 0;
					figures[countFigure].X[i] += 2;
				}
				for (int i = 0; i < sizeFigure; i++)
				{
					map[figures[countFigure].Y[i]][figures[countFigure].X[i]] = figures[countFigure].number;
				}
			}
			break;
		case 'a':
			countCollision = сollisionLeft(countCollision);
			if (countCollision == 0)
			{
				for (int i = 0; i < sizeFigure; i++)
				{
					map[figures[countFigure].Y[i]][figures[countFigure].X[i]] = 0;
					figures[countFigure].X[i] -= 2;
				}
				for (int i = 0; i < sizeFigure; i++)
				{
					map[figures[countFigure].Y[i]][figures[countFigure].X[i]] = figures[countFigure].number;
				}
			}
			break;
		case 'w':
			countCollision = сollisionText(countCollision);
			if (countCollision == 0)
			{
				for (int i = 0; i < sizeFigure; i++)
				{
					map[figures[countFigure].Y[i]][figures[countFigure].X[i]] = 0;
				}
				orientationFigure();
				for (int i = 0; i < sizeFigure; i++)
				{
					map[figures[countFigure].Y[i]][figures[countFigure].X[i]] = figures[countFigure].number;
				}
			}
			break;
		case 'f':
			saveGame();
			break;
		default:
			break;
		}
	}
	else
	{
		countCollision = сollisionDown(countCollision);
		if (countCollision == 0)
		{
			for (int i = 0; i < sizeFigure; i++)
			{
				map[figures[countFigure].Y[i]][figures[countFigure].X[i]] = 0;
				figures[countFigure].Y[i]++;
			}
			for (int i = 0; i < sizeFigure; i++)
			{
				map[figures[countFigure].Y[i]][figures[countFigure].X[i]] = figures[countFigure].number;
			}
		}
		else if (countCollision != 0)
		{
			countFigure++;
			score += 10;
			checkLine();
			figureRand();
		}
	}
}

void game() {
	while (true)
	{
		showGame();
		keystrokesGame();
		Sleep(200);
		system("cls");
	}
}